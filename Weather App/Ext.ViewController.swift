//
//  Ext.swift
//  Weather App
//
//  Created by Afriyandi Setiawan on 18/10/17.
//  Copyright © 2017 Afriyandi Setiawan. All rights reserved.
//

import Foundation
import UIKit

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.jsonDetail?.count ?? 0
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detail", for: indexPath) as? CellDetailsTable
        let head = Array((self.jsonDetail ?? [:]).keys)
        cell?.detailsKey.text = head[indexPath.item]
        cell?.detailsValue.text = self.jsonDetail?[head[indexPath.item]]
        return cell ?? UITableViewCell()
    }
}

extension ViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.scrollView, scrollView.contentOffset.y > 0 {
            scrollView.contentOffset.y = 0
        }
    }
}
