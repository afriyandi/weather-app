//
//  DetailsTable.swift
//  Weather App
//
//  Created by Afriyandi Setiawan on 18/10/17.
//  Copyright © 2017 Afriyandi Setiawan. All rights reserved.
//

import UIKit

class DetailsTable: UITableView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}


class CellDetailsTable: UITableViewCell {
    @IBOutlet weak var detailsKey: UILabel!
    @IBOutlet weak var detailsValue: UILabel!    
}
