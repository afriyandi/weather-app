//
//  ViewController.swift
//  Weather App
//
//  Created by Afriyandi Setiawan on 16/10/17.
//  Copyright © 2017 Afriyandi Setiawan. All rights reserved.
//

import UIKit
import CoreLocation
import PheConnector
import SwiftyJSON


class ViewController: UIViewController {
    
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var currentTemperature: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var temperatureText: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var detailTable: DetailsTable!
    var isAuthorizeLocation:Bool?
    var locationManager: CLLocationManager?
    var mettricOrImperial:String?
    var refreshControl:UIRefreshControl?
    var jsonCurrentCondition:JSON?
    var jsonDetail:Dictionary<String, String>?
    lazy var dateFormater:DateFormatter = {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = DateFormatter.dateFormat(fromTemplate: "yyyy-MM-dd'T'HH:mm:ss", options: 0, locale: Locale.current)
        return dateFormat
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.distanceFilter = 100
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        refreshControl?.attributedTitle = NSAttributedString(string: "refresh")
        self.mettricOrImperial = "Metric"
        self.scrollView.addSubview(refreshControl!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkPermission()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showDetail(_ sender: UIButton) {
    }
    
    @IBAction func changeMetric(_ sender: UITapGestureRecognizer) {
        //need better way to implement this, 2 days only though
        self.mettricOrImperial = self.mettricOrImperial == "Metric" ? "Imperial" : "Metric"
        self.currentTemperature.text = String(describing: self.jsonCurrentCondition?[0]["Temperature"][self.mettricOrImperial ?? "Metric"]["Value"].int ?? 0)
        self.temperatureLabel.text = self.mettricOrImperial == "Metric" ? "℃" : "℉"
        self.constructDataForTable { 
            self.detailTable.reloadData()
        }
    }
    
    func checkPermission() {
        let locationPermission = CLLocationManager.authorizationStatus()
        switch locationPermission {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager?.startUpdatingLocation()
        case .notDetermined:
            let isLocationEnabled = CLLocationManager.locationServicesEnabled()
            if isLocationEnabled {
                locationManager?.requestWhenInUseAuthorization()
                locationManager?.requestAlwaysAuthorization()
            }
        default:
            let alert = UIAlertController(title: "", message: "Please Enable location", preferredStyle: .alert)
            alert.showToast(animated: true, completion: {})
        }
    }
    
    func getLocationKey(coordinate:CLLocationCoordinate2D) {
        let services = Services<V>.getLocationKey(url: "cities/geoposition/search")
        let parameter = addCustomParameter(withItem: [["q": "\(coordinate.latitude),\(coordinate.longitude)" as AnyObject]])
        let endPoint = EndPoint(url: services.url, method: services.method, parameter: parameter, customHeader: DefaultHeader)
        phe.routine(endPoint, message: { (success, message) in
            if success {
                //TODO: error handler request location key or change Pheconnector handle data response
                if success, let _message = message as? Data {
                    let _json = JSON(data: _message)
                    self.location.text = _json["LocalizedName"].stringValue
                    self.city.text = _json["AdministrativeArea"]["LocalizedName"].stringValue
                    self.getCurrentCondition(locationKey: _json["Key"].stringValue)
                    let _timeZone = _json["TimeZone"]["Name"].stringValue
                    self.dateFormater.timeZone = TimeZone(identifier: _timeZone)
                }
            }
        })
    }
    
    func getCurrentCondition(locationKey: String?) {
        
        let services = Services<V>.getCurrentCondition(locationKey: locationKey)
        let parameter = addCustomParameter(withItem: [["details":"true" as AnyObject]])
        let endPoint = EndPoint(url: services.url, method: services.method, parameter: parameter, customHeader: DefaultHeader)
        
        phe.routine(endPoint) { (success, message) in
            // direct parsing, should make a parse method later
            if success, let _message = message as? Data {
                self.jsonCurrentCondition = JSON(data: _message)
                if let code = self.jsonCurrentCondition?["Code"].string, code == "ServiceUnavailable"  {
                    let alert = UIAlertController(title: self.jsonCurrentCondition?["Code"].stringValue, message: self.jsonCurrentCondition?["Message"].stringValue, preferredStyle: .alert)
                    alert.showToast(animated: true, completion:{})
                    return
                }
                self.currentTemperature.text = String(describing: self.jsonCurrentCondition?[0]["Temperature"][self.mettricOrImperial ?? "Metric"]["Value"].int ?? 0)
                self.temperatureLabel.text = self.mettricOrImperial == "Metric" ? "℃" : "℉"
                self.temperatureText.text = self.jsonCurrentCondition?[0]["WeatherText"].stringValue
                let timeInterval = self.jsonCurrentCondition?[0]["EpochTime"].double ?? 0.0
                let _date = Date(timeIntervalSince1970: timeInterval)
                self.dateTime.text = self.dateFormater.string(from: _date)
                let imageID = self.jsonCurrentCondition?[0]["WeatherIcon"].numberValue
                let formater = NumberFormatter()
                formater.minimumIntegerDigits = 2
                self.getImageForCondition(imageID: String(describing:formater.string(from: imageID ?? 1) ?? "01"))
                self.constructDataForTable {
                    self.detailTable.reloadData()
                }
            }
        }
    }
    
    func getImageForCondition(imageID: String?) {
        let services = Services<V>.getImageForCondition(url: "sites/default/files", idImage: imageID)
        let endPoint = EndPoint(url: services.url, method: services.method, parameter: nil, customHeader: DefaultHeader)
        phe.download(endPoint) { (success, imageData) in
            if success {
                self.weatherIcon.image = UIImage(data: imageData ?? Data())
            }
            if let isRefreshing = self.refreshControl?.isRefreshing, isRefreshing {
                self.refreshControl?.endRefreshing()
            }
        }
    }
    
    func refreshData() {
        self.refreshControl?.beginRefreshing()
        self.locationManager?.startUpdatingLocation()
    }
    
    func constructDataForTable(finish: ()->()) {
        self.jsonDetail = [
            "Wind": String(format: "%@, at %.2f%@",
                           self.jsonCurrentCondition?[0]["Wind"]["Direction"]["Localized"].string ?? "",
                           self.jsonCurrentCondition?[0]["Wind"]["Speed"][self.mettricOrImperial ?? "Metric"]["Value"].double ?? "",
                           self.jsonCurrentCondition?[0]["Wind"]["Speed"][self.mettricOrImperial ?? "Metric"]["Unit"].string ?? ""),
            "Humidity": String(format: "%d%%", self.jsonCurrentCondition?[0]["RelativeHumidity"].int ?? 0),
            "Cloud Cover": String(format: "%d%%", self.jsonCurrentCondition?[0]["CloudCover"].int ?? 0),
            "UV Index": self.jsonCurrentCondition?[0]["UVIndexText"].string ?? ""
        ]
        finish()
    }
    
}

extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            isAuthorizeLocation = true
            checkPermission()
            return
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let latestLocation: CLLocation = locations.last!
        getLocationKey(coordinate: CLLocationCoordinate2D(latitude: latestLocation.coordinate.latitude, longitude: latestLocation.coordinate.longitude))

    }
}
