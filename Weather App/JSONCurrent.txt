[
{
"LocalObservationDateTime": "2017-10-17T05:40:00+07:00",
"EpochTime": 1508193600,
"WeatherText": "Light fog",
"WeatherIcon": 11,
"IsDayTime": true,
"Temperature": {
"Metric": {
"Value": 25,
"Unit": "C",
"UnitType": 17
},
"Imperial": {
"Value": 77,
"Unit": "F",
"UnitType": 18
}
},
"RealFeelTemperature": {
"Metric": {
"Value": 30.3,
"Unit": "C",
"UnitType": 17
},
"Imperial": {
"Value": 86,
"Unit": "F",
"UnitType": 18
}
},
"RealFeelTemperatureShade": {
"Metric": {
"Value": 30.3,
"Unit": "C",
"UnitType": 17
},
"Imperial": {
"Value": 86,
"Unit": "F",
"UnitType": 18
}
},
"RelativeHumidity": 94,
"DewPoint": {
"Metric": {
"Value": 23.9,
"Unit": "C",
"UnitType": 17
},
"Imperial": {
"Value": 75,
"Unit": "F",
"UnitType": 18
}
},
"Wind": {
"Direction": {
"Degrees": 0,
"Localized": "N",
"English": "N"
},
"Speed": {
"Metric": {
"Value": 1.9,
"Unit": "km/h",
"UnitType": 7
},
"Imperial": {
"Value": 1.2,
"Unit": "mi/h",
"UnitType": 9
}
}
},
"WindGust": {
"Speed": {
"Metric": {
"Value": 1.9,
"Unit": "km/h",
"UnitType": 7
},
"Imperial": {
"Value": 1.2,
"Unit": "mi/h",
"UnitType": 9
}
}
},
"UVIndex": 0,
"UVIndexText": "Low",
"Visibility": {
"Metric": {
"Value": 4.8,
"Unit": "km",
"UnitType": 6
},
"Imperial": {
"Value": 3,
"Unit": "mi",
"UnitType": 2
}
},
"ObstructionsToVisibility": "F",
"CloudCover": 20,
"Ceiling": {
"Metric": {
"Value": 6949,
"Unit": "m",
"UnitType": 5
},
"Imperial": {
"Value": 22800,
"Unit": "ft",
"UnitType": 0
}
},
"Pressure": {
"Metric": {
"Value": 1009,
"Unit": "mb",
"UnitType": 14
},
"Imperial": {
"Value": 29.8,
"Unit": "inHg",
"UnitType": 12
}
},
"PressureTendency": {
"LocalizedText": "Steady",
"Code": "S"
},
"Past24HourTemperatureDeparture": {
"Metric": {
"Value": 0,
"Unit": "C",
"UnitType": 17
},
"Imperial": {
"Value": 0,
"Unit": "F",
"UnitType": 18
}
},
"ApparentTemperature": {
"Metric": {
"Value": 27.8,
"Unit": "C",
"UnitType": 17
},
"Imperial": {
"Value": 82,
"Unit": "F",
"UnitType": 18
}
},
"WindChillTemperature": {
"Metric": {
"Value": 25,
"Unit": "C",
"UnitType": 17
},
"Imperial": {
"Value": 77,
"Unit": "F",
"UnitType": 18
}
},
"WetBulbTemperature": {
"Metric": {
"Value": 24.3,
"Unit": "C",
"UnitType": 17
},
"Imperial": {
"Value": 76,
"Unit": "F",
"UnitType": 18
}
},
"Precip1hr": {
"Metric": {
"Value": 0,
"Unit": "mm",
"UnitType": 3
},
"Imperial": {
"Value": 0,
"Unit": "in",
"UnitType": 1
}
},
"PrecipitationSummary": {
"Precipitation": {
"Metric": {
"Value": 0,
"Unit": "mm",
"UnitType": 3
},
"Imperial": {
"Value": 0,
"Unit": "in",
"UnitType": 1
}
},
"PastHour": {
"Metric": {
"Value": 0,
"Unit": "mm",
"UnitType": 3
},
"Imperial": {
"Value": 0,
"Unit": "in",
"UnitType": 1
}
},
"Past3Hours": {
"Metric": {
"Value": 0,
"Unit": "mm",
"UnitType": 3
},
"Imperial": {
"Value": 0,
"Unit": "in",
"UnitType": 1
}
},
"Past6Hours": {
"Metric": {
"Value": 0,
"Unit": "mm",
"UnitType": 3
},
"Imperial": {
"Value": 0,
"Unit": "in",
"UnitType": 1
}
},
"Past9Hours": {
"Metric": {
"Value": 0,
"Unit": "mm",
"UnitType": 3
},
"Imperial": {
"Value": 0,
"Unit": "in",
"UnitType": 1
}
},
"Past12Hours": {
"Metric": {
"Value": 0,
"Unit": "mm",
"UnitType": 3
},
"Imperial": {
"Value": 0,
"Unit": "in",
"UnitType": 1
}
},
"Past18Hours": {
"Metric": {
"Value": 1,
"Unit": "mm",
"UnitType": 3
},
"Imperial": {
"Value": 0.03,
"Unit": "in",
"UnitType": 1
}
},
"Past24Hours": {
"Metric": {
"Value": 1,
"Unit": "mm",
"UnitType": 3
},
"Imperial": {
"Value": 0.03,
"Unit": "in",
"UnitType": 1
}
}
},
"TemperatureSummary": {
"Past6HourRange": {
"Minimum": {
"Metric": {
"Value": 25,
"Unit": "C",
"UnitType": 17
},
"Imperial": {
"Value": 77,
"Unit": "F",
"UnitType": 18
}
},
"Maximum": {
"Metric": {
"Value": 26.1,
"Unit": "C",
"UnitType": 17
},
"Imperial": {
"Value": 79,
"Unit": "F",
"UnitType": 18
}
}
},
"Past12HourRange": {
"Minimum": {
"Metric": {
"Value": 25,
"Unit": "C",
"UnitType": 17
},
"Imperial": {
"Value": 77,
"Unit": "F",
"UnitType": 18
}
},
"Maximum": {
"Metric": {
"Value": 27.8,
"Unit": "C",
"UnitType": 17
},
"Imperial": {
"Value": 82,
"Unit": "F",
"UnitType": 18
}
}
},
"Past24HourRange": {
"Minimum": {
"Metric": {
"Value": 25,
"Unit": "C",
"UnitType": 17
},
"Imperial": {
"Value": 77,
"Unit": "F",
"UnitType": 18
}
},
"Maximum": {
"Metric": {
"Value": 33.8,
"Unit": "C",
"UnitType": 17
},
"Imperial": {
"Value": 93,
"Unit": "F",
"UnitType": 18
}
}
}
},
"MobileLink": "http://m.accuweather.com/en/id/kenari/1982868/current-weather/1982868?lang=en-us",
"Link": "http://www.accuweather.com/en/id/kenari/1982868/current-weather/1982868?lang=en-us"
}
]
