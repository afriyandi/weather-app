{
"Version":1,
"Key":"1915836",
"Type":"City",
"Rank":65,
"LocalizedName":"Paseban",
"EnglishName":"Paseban",
"PrimaryPostalCode":"",
"Region":{
"ID":"ASI",
"LocalizedName":"Asia",
"EnglishName":"Asia"
},
"Country":{
"ID":"ID",
"LocalizedName":"Indonesia",
"EnglishName":"Indonesia"
},
"AdministrativeArea":{
"ID":"JK",
"LocalizedName":"Jakarta",
"EnglishName":"Jakarta",
"Level":1,
"LocalizedType":"Special Capital City District",
"EnglishType":"Special Capital City District",
"CountryID":"ID"
},
"TimeZone":{
"Code":"WIT",
"Name":"Asia/Jakarta",
"GmtOffset":7,
"IsDaylightSaving":false,
"NextOffsetChange":null
},
"GeoPosition":{
"Latitude":-6.193,
"Longitude":106.853,
"Elevation":{
"Metric":{
"Value":5,
"Unit":"m",
"UnitType":5
},
"Imperial":{
"Value":17,
"Unit":"ft",
"UnitType":0
}
}
},
"IsAlias":false,
"ParentCity":{
"Key":"1982856",
"LocalizedName":"Jakarta Pusat",
"EnglishName":"Jakarta Pusat"
},
"SupplementalAdminAreas":[
{
"Level":2,
"LocalizedName":"Jakarta Pusat",
"EnglishName":"Jakarta Pusat"
},
{
"Level":3,
"LocalizedName":"Senen",
"EnglishName":"Senen"
}
],
"DataSets":[

]
}
