//
//  Services.swift
//  Weather App
//
//  Created by Afriyandi Setiawan on 17/10/17.
//  Copyright © 2017 Afriyandi Setiawan. All rights reserved.
//

import PheConnector

let apiKey = "cdfH1oYVFjKPT4NY1dz3pimk40o7skC7"

protocol VersionNumber {
    static var version: String { get }
}

struct V: VersionNumber {
    static var version: String { return "v1" }
}


enum Services<T> where T: VersionNumber {
    case getLocationKey(url: String)
    case getCurrentCondition(locationKey: String?)
    case getImageForCondition(url: String, idImage:String?)
    
    // MARK: - Public Properties
    var method: HTTPMethod {
        switch self {
        case .getLocationKey, .getCurrentCondition, .getImageForCondition: return .get
        }
    }
    
    var version: String {
        return T.version
    }
    
    var baseURL: URL {
        switch self {
        case .getLocationKey, .getCurrentCondition:
            return URL.getBaseUrl()
        case .getImageForCondition:
            return URL.getImageURL()
        }
    }
    
    var url: URL {
        switch self {
        case .getLocationKey(let url):
            return self.baseURL.appendingPathComponent("locations/\(version)/\(url)")
        case .getCurrentCondition(let locationKey):
            return self.baseURL.appendingPathComponent("currentconditions/\(version)/\(locationKey ?? "")")
        case .getImageForCondition(let url, let idImage):
            return self.baseURL.appendingPathComponent("\(url)/\(idImage ?? "01")-s.png")            
        }
    }
}

extension URL {
    static func getBaseUrl() -> URL {
        guard let info = Bundle.main.infoDictionary,
            let urlString = info["Base URL"] as? String,
            let url = URL(string: urlString) else {
                fatalError("Cannot get base url from info.plist")
        }
        return url
    }
    
    static func getImageURL() -> URL {
        guard let info = Bundle.main.infoDictionary,
            let urlString = info["Image URL"] as? String,
            let url = URL(string: urlString) else {
                fatalError("Cannot get base url from info.plist")
        }
        return url
    }
}

let DefaultHeader:Dictionary<String, String> = {
    return ["Accept-Encoding":"gzip", "User-Agent":"weather app/\(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String) (weather app for \(UIDevice.current.model); iOS version \(UIDevice.current.systemVersion)) ~ phe"]
}()

let DefaultParameter:Dictionary<String, AnyObject> = {
    return ["apikey": apiKey as AnyObject]
}()


func addCustomParameter(withItem params: Array<Dictionary<String, AnyObject>>) -> Dictionary<String, AnyObject> {
    var customParameter = Dictionary<String, AnyObject>()
    for param in params {
        customParameter += param
    }
    customParameter += DefaultParameter
    return customParameter
}

func addCustomHeader(withItem headers: Array<Dictionary<String, String>>) -> Dictionary<String, String> {
    var customHeader = Dictionary<String, String>()
    for head in headers {
        customHeader += head
    }
    customHeader += DefaultHeader
    return customHeader
}

var phe = Phe()

func += <K, V> (left: inout [K : V], right: [K : V]) {
    for (k, v) in right {
        left.updateValue(v, forKey: k)
    }
}
