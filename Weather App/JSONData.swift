//
//  JSONData.swift
//  Weather App
//
//  Created by Afriyandi Setiawan on 18/10/17.
//  Copyright © 2017 Afriyandi Setiawan. All rights reserved.
//

import Foundation
import SwiftyJSON

//To avoid limit, testing purpose only

func jsonData(fileName: String) -> JSON? {
    guard let path = Bundle.main.path(forResource: fileName, ofType: "txt") else {
        return nil
    }
    
    do {
        let content = try String(contentsOfFile:path, encoding: String.Encoding.utf8)        
        return JSON(data: content.data(using: String.Encoding.utf8)!)
    } catch _ as NSError {
        return nil
    }
}
