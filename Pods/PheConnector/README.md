# PheConnector

[![CI Status](http://img.shields.io/travis/Afriyandi Setiawan/PheConnector.svg?style=flat)](https://travis-ci.org/Afriyandi Setiawan/PheConnector)
[![Version](https://img.shields.io/cocoapods/v/PheConnector.svg?style=flat)](http://cocoapods.org/pods/PheConnector)
[![License](https://img.shields.io/cocoapods/l/PheConnector.svg?style=flat)](http://cocoapods.org/pods/PheConnector)
[![Platform](https://img.shields.io/cocoapods/p/PheConnector.svg?style=flat)](http://cocoapods.org/pods/PheConnector)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PheConnector is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "PheConnector"
```

## Author

Afriyandi Setiawan, afreeyandi@gmail.com

## License

PheConnector is available under the MIT license. See the LICENSE file for more info.
