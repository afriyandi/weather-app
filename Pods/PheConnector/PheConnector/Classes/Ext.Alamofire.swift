//
//  Extension.Alamofire.swift
//  Pods
//
//  Created by Afriyandi Setiawan on 8/4/17.
//
//

import Foundation
import Alamofire

extension SessionManager: APIManagerProtocol {

    func apiRequest(_ endPoint: EndPoint) -> ApiRequestProtocol {
        
        let commonHeaders = appendHeader(headers: endPoint.customHeader ?? Dictionary<String, String>())
        
        return request(endPoint.url, method: Alamofire.HTTPMethod(rawValue: endPoint.method.rawValue) ?? .get, parameters: endPoint.parameter, headers: commonHeaders)
    }
    
    func upload(_ endPoint: EndPoint, image: ImageProperty, result: @escaping (Bool , Data?) -> ()) {
        let commonHeaders = appendHeader(headers: endPoint.customHeader ?? Dictionary<String, String>())
        Alamofire.upload(multipartFormData: { (form) in
            form.append(image.image, withName: image.formName, fileName: image.imageName, mimeType: image.mimeType)
            if let param = endPoint.parameter {
                for (key, value) in param {
                    form.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            }
        }, to: endPoint.url, headers: commonHeaders, encodingCompletion: { (_message) in
            switch _message {
            case .success(let _upload, _, _):
                _upload.responseJSON(completionHandler: { (completion) in
                    switch completion.result {
                    case .success:
                        let countBytes = ByteCountFormatter()
                        countBytes.allowedUnits = [.useMB]
                        countBytes.countStyle = .file
                        let prompt = UIAlertController(title: "debugger 2", message: completion.timeline.description + "\nfile size: \(countBytes.string(fromByteCount: Int64(image.image.count)))", preferredStyle: .alert)
                        let ok = UIAlertAction(title: "OK", style: .destructive, handler: { _ in
                            return result(true, completion.data)
                        })
                        prompt.addAction(ok)
                        prompt.showCustom()
                    case .failure:
                        return result(false, nil)
                    }
                })
            case .failure( _):
                result(false, nil)
                return
            }
        })
    }
    
    func download(_ endPoint: EndPoint) -> DownloadRequestProtocol {
        let commonHeaders = appendHeader(headers: endPoint.customHeader ?? Dictionary<String, String>())
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let documentsURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
            let fileURL = documentsURL.appendingPathComponent("\(Date().timeIntervalSince1970).tmp")
            
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        return Alamofire.download(endPoint.url, parameters: endPoint.parameter, headers: commonHeaders, to: destination)
    }
    
    func appendHeader(headers: Dictionary<String, String>) -> Dictionary<String, String> {
        if headers.count > 0 {
            var commonHeaders = [String:String]()
            commonHeaders += headers
            return commonHeaders
        }
        return headers
    }
}

extension DataRequest: ApiRequestProtocol {
    func apiResponse(_ completionHandler: @escaping (DataResponse<Any>) -> Void) -> Self {
        return responseJSON(completionHandler: completionHandler)
    }
}

extension DownloadRequest: DownloadRequestProtocol {
    func downloadResponse(_ completionHandler: @escaping (DownloadResponse<Data>) -> Void) -> Self {
        return responseData(completionHandler: completionHandler)
    }
}

