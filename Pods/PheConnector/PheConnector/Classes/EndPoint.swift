//
//  EndPoint.swift
//  Pods
//
//  Created by Afriyandi Setiawan on 8/3/17.
//
//

import Foundation
import Alamofire

public struct EndPoint {
    var url: URL!
    var method: HTTPMethod
    var parameter: Dictionary<String, AnyObject>?
    var customHeader: Dictionary<String, String>?
    
    public init(url: URL, method: HTTPMethod, parameter: Dictionary<String, AnyObject>?, customHeader: Dictionary<String, String>?) {
        self.url = url
        self.method = method
        self.parameter = parameter
        self.customHeader = customHeader
    }
    
    init(url: URL, method: HTTPMethod, parameter: Dictionary<String, AnyObject>?) {
        self.url = url
        self.method = method
        self.parameter = parameter
    }
}

public struct ImageProperty {
    var formName, imageName, mimeType: String
    var image: Data
    
    public init(formName: String, image: Data, imageName: String, mimeType: String) {
        self.formName = formName
        self.image = image
        self.imageName = imageName
        self.mimeType = mimeType
    }

}

protocol APIManagerProtocol {
    func apiRequest(_ endPoint: EndPoint) -> ApiRequestProtocol
    func upload(_ endPoint: EndPoint, image: ImageProperty, result: @escaping (Bool , Data?) -> ())
    func download(_ endPoint: EndPoint) -> DownloadRequestProtocol
}

protocol ApiRequestProtocol {
    func apiResponse(_ completionHandler: @escaping (DataResponse<Any>) -> Void) -> Self
}

protocol DownloadRequestProtocol {
    func downloadResponse(_ completionHandler: @escaping (DownloadResponse<Data>) -> Void) -> Self
}

func += <K, V> (left: inout [K : V], right: [K : V]) {
    for (k, v) in right {
        left.updateValue(v, forKey: k)
    }
}
