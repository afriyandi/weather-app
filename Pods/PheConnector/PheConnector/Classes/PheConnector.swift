//
//  PheConnector.swift
//  Pods
//
//  Created by Afriyandi Setiawan on 8/1/17.
//
//

import Foundation
import Alamofire
import CryptoSwift

//o2o journal requirement
public struct JSONHash {
    public static var hashValue = String()
    func calculateTheHash(_ json:String) {
        JSONHash.hashValue = json.md5()
    }
}

public struct Phe {
    
    static var manager: SessionManager = {
        let a = URLSessionConfiguration.default
        a.allowsCellularAccess = false
        return SessionManager(configuration: a)
    }()
        
    public init() {
        Phe.manager = SessionManager(configuration: URLSessionConfiguration.default)
    }
    
    public func routine(_ endPoint: EndPoint, message: @escaping (Bool, Any) -> Void) {
        _ = Phe.manager.apiRequest(endPoint).apiResponse { (_message) in
            switch _message.result {
            case .success:
                return message(true, _message.data ?? Data())
            case .failure:
                return message(false, _message.error as AnyObject)
            }
        }
    }
    
    public func upload(_ endPoint: EndPoint, imageParameter: ImageProperty, message: @escaping (Bool, Any) -> Void) {
        Phe.manager.upload(endPoint, image: imageParameter) { (s, prot) -> () in
            if s, let imagedata = prot {
                message(true, self.parserResult(imagedata))
            }
        }
    }
    
    public func download(_ endPoint: EndPoint, message: @escaping (Bool, Data?) -> Void) {
        _ = Phe.manager.download(endPoint).downloadResponse { (response) in
            switch response.result {
            case .success:
                if response.error == nil, let filePath = response.destinationURL {
                    guard let data = try? Data(contentsOf: filePath) else {
                        return message(false, nil)
                    }
                    return message(true, data)
                }
                return message(false, nil)
            case .failure:
                return message(false, nil)
            }
        }
    }
    
    public func clearCache(){
        let cacheURL =  FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        let fileManager = FileManager.default
        do {
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try FileManager.default.contentsOfDirectory( at: cacheURL, includingPropertiesForKeys: nil, options: [])
            let tmpFIle = directoryContents.filter({ (file) -> Bool in
                return file.pathExtension == "tmp"
            })
            for file in tmpFIle {
                do {
                    try fileManager.removeItem(at: file)
                }
                catch let error as NSError {
                    debugPrint("Ooops! Something went wrong: \(error)")
                }
                
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    func parserResult(_ message: Data) -> Any {
        var responseStr:String = String()
        
        //return as what it is
        guard let rsp:String = NSString(data: message as Data, encoding: String.Encoding.utf8.rawValue) as String? else {
            return message as AnyObject
        }
        
        responseStr = rsp
        var JSON:Any?
        do {
            JSON = try JSONSerialization.jsonObject(with: message as Data!, options:[])
            guard let _ :Array<String> = JSON as? Array<String> else {
                let hash = JSONHash()
                hash.calculateTheHash(responseStr)
                return JSON as! Dictionary<String, AnyObject>
            }
            return JSON as! Array<String>
        }
        catch let JSONError as NSError {
            print("\(JSONError)")
        }
        catch {
            print("hell if i know")
        }
        return responseStr as AnyObject
    }
}

extension UIAlertController {
    
    func showCustom() {
        presentCustom(animated: true, completion: nil)
    }
    
    func presentCustom(animated: Bool, completion: (() -> Void)?) {
        if let rootVC = UIApplication.shared.keyWindow?.visibleViewController {
            presentFromController(controller: rootVC, animated: animated, completion: completion)
        }
    }
    
    private func presentFromController(controller: UIViewController, animated: Bool, completion: (() -> Void)?) {
        controller.definesPresentationContext = true
        switch controller {
        case let navVC as UINavigationController:
            presentFromController(controller: navVC.visibleViewController!, animated: animated, completion: completion)
            break
        case let tabVC as UITabBarController:
            presentFromController(controller: tabVC.selectedViewController!, animated: animated, completion: completion)
        case let presented where (controller.presentedViewController != nil):
            presented.present(self, animated: animated, completion: completion)
        default:
            controller.present(self, animated: animated, completion: completion)
        }
    }
}

extension UIWindow {
    public var visibleViewController: UIViewController? {
        return UIWindow.visibleViewController(from: rootViewController)
    }
    public static func visibleViewController(from viewController: UIViewController?) -> UIViewController? {
        switch viewController {
        case let navigationController as UINavigationController:
            return UIWindow.visibleViewController(from: navigationController.visibleViewController)
            
        case let tabBarController as UITabBarController:
            return UIWindow.visibleViewController(from: tabBarController.selectedViewController)
            
        case let presentingViewController where viewController?.presentedViewController != nil:
            return UIWindow.visibleViewController(from: presentingViewController?.presentedViewController)
            
        default:
            return viewController
        }
    }
}
